package com.example.photoapp;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class PhotoListFragment extends Fragment {
    private ImageView iv1;
    private ImageView iv2;
    private ImageView iv3;
    private ImageView iv4;
    private ImageView iv5;
    private ImageView iv6;
    private ArrayList<ImageView> imageViews = new ArrayList<>();
    private ArrayList<String> listOfURLS;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.photo_list_fragment,container,false);
        iv1 = view.findViewById(R.id.photolistImageView1);
        iv2 = view.findViewById(R.id.photolistImageView2);
        iv3 = view.findViewById(R.id.photolistImageView3);
        iv4 = view.findViewById(R.id.photolistImageView4);
        iv5 = view.findViewById(R.id.photolistImageView5);
        iv6 = view.findViewById(R.id.photolistImageView6);
        addImageViewToArray();
        getData();

        int numberOfPhotosToLoad = listOfURLS.size()> 6 ? 6 : listOfURLS.size();

        for(int i=0;i<numberOfPhotosToLoad;i++){
            Picasso.get().load(listOfURLS.get(i)).into(imageViews.get(i));
        }

        return view;
    }

    private void addImageViewToArray() {
        imageViews.add(iv1);
        imageViews.add(iv2);
        imageViews.add(iv3);
        imageViews.add(iv4);
        imageViews.add(iv5);
        imageViews.add(iv6);

    }

    private void getData() {
        listOfURLS = getArguments().getStringArrayList("URLS");
    }
}
