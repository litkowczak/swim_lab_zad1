package com.example.photoapp.logic;


import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import com.google.firebase.ml.vision.label.FirebaseVisionImageLabel;
import com.google.firebase.ml.vision.label.FirebaseVisionImageLabeler;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.SimpleTimeZone;


public class PhotoData {
    public void setTagsArray(ArrayList<String> tags) {
        this.tags = tags;
    }
    public void addTagArray(String tag){
        tags.add(tag);
    }

    private static final String SIGN = "#";
    private String imgUrl;
    private String name;
    private ArrayList<String> tags;
    private String date;

    public PhotoData(String imgUrl, String name) {
        this.imgUrl = imgUrl;
        this.name = name;
        this.tags = new ArrayList<String>();

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        this.date = sdf.format(new Date());


    }

    public String getImgUrl() {
        return imgUrl;
    }

    public String getName() {
        return name;
    }

    public String getDate() {
        return date;
    }

    public ArrayList<String> getTags() {
        return tags;
    }

    public String getTag() {
        String result = "";
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < (tags.size() <= 3 ? tags.size() : 3); i++) {
            sb.append(SIGN).append(tags.get(i));
        }
        return result = sb.toString();
    }


   public void generateTags() {
        Picasso.get().load(this.imgUrl).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                Log.wtf("TAG", "ladowanie bitmap");
                setTags(bitmap);
            }

            @Override
            public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                Log.wtf("TAG", "ERROR");
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
                Log.wtf("TAG","Preparing for load");
            }
        });
    }

    public void setTags(Bitmap bitmap) {

        FirebaseVisionImage fvi = FirebaseVisionImage.fromBitmap(bitmap);
        FirebaseVisionImageLabeler labeler = FirebaseVision.getInstance().getOnDeviceImageLabeler();
        labeler.processImage(fvi)
                .addOnSuccessListener(new OnSuccessListener<List<FirebaseVisionImageLabel>>() {
                    @Override
                    public void onSuccess(List<FirebaseVisionImageLabel> labels) {
                        for (FirebaseVisionImageLabel label : labels) {
                            Log.wtf("TAG", label.getText());
                            tags.add(label.getText());
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        // Task failed with an exception
                        // ...
                        Log.wtf("TAG", "Set tag fail");
                    }
                });
    }


}
