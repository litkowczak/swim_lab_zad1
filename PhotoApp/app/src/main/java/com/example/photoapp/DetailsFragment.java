package com.example.photoapp;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import java.util.ArrayList;

public class DetailsFragment extends Fragment {
    private String name;
    private ArrayList<String> tags;
    private TextView nameTV;
    private TextView tagsTV;
    private FrameLayout frameLayout;
    private ArrayList<String> listOfURLS = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_details, container, false);
        nameTV = view.findViewById(R.id.fragmentNameTV);
        tagsTV = view.findViewById(R.id.fragmentTagsTV);
        getData();
        nameTV.setText(name);
        tagsTV.setText(getTags());
        frameLayout = view.findViewById(R.id.fragmentFrameLayout);

        PhotoListFragment fragment = new PhotoListFragment();
        Bundle bundle = new Bundle();
        bundle.putStringArrayList("URLS",listOfURLS);
        fragment.setArguments(bundle);

        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.add(frameLayout.getId(),fragment);
        ft.commit();
        return view;
    }

    private void getData() {
        name = getArguments().getString("NAME");
        tags = getArguments().getStringArrayList("TAGS");
        listOfURLS = getArguments().getStringArrayList("URLSLIST");
    }
    private String getTags(){

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < tags.size(); i++) {
            sb.append("#").append(tags.get(i));
        }
        return  sb.toString();
    }

}

