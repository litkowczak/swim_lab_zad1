package com.example.photoapp;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.example.photoapp.logic.PhotoData;

import java.util.ArrayList;
import java.util.TooManyListenersException;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerView.Adapter recyclerViewAdapter;
    private RecyclerView.LayoutManager recyclerViewLM;
    private ArrayList<PhotoData> photoDataArrayList = new ArrayList<PhotoData>();

    private String url1 = "http://therightsofnature.org/wp-content/uploads/2018/01/turkey-3048299_1920.jpg";
    private String url2 = "https://ki.se/sites/default/files/styles/adaptive/public/2018/10/19/istock_host_autumn_fall_water.jpg?itok=j4g1ciLW";
    private String url3 = "https://upload.wikimedia.org/wikipedia/commons/1/17/WSK_B1.jpg";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        /*photoDataArrayList.add(new PhotoData(url1,"Natura 1"));
        photoDataArrayList.add(new PhotoData(url2,"Natura Siema"));
        photoDataArrayList.add(new PhotoData(url3,"WSK"));*/

        setUpRecyclerView();


    }

    private void setUpRecyclerView() {
        recyclerView = (RecyclerView) findViewById(R.id.recyclerViewMain);
        recyclerViewLM = new LinearLayoutManager(this);
        recyclerView.setLayoutManager( recyclerViewLM);

        recyclerViewAdapter = new PhotoDataAdapter(photoDataArrayList, this);
        ((PhotoDataAdapter) recyclerViewAdapter).setOnItemClickListener(new PhotoDataAdapter.ClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                Intent intent = new Intent(v.getContext(),DetailsActivity.class);
                intent.putExtra("URL",photoDataArrayList.get(position).getImgUrl());
                intent.putExtra("NAME",photoDataArrayList.get(position).getName());
                intent.putExtra("TAGS",photoDataArrayList.get(position).getTags());
                intent.putExtra("NUMBEROFPHOTOS", photoDataArrayList.size());
                intent.putExtra("POSITION",position);
                for(int i=0;i<photoDataArrayList.size();i++){
                    intent.putExtra("TAGLIST"+i,photoDataArrayList.get(i).getTags());
                    intent.putExtra("URLLIST"+i,photoDataArrayList.get(i).getImgUrl());
                }

                startActivity(intent);
            }
        });


        recyclerView.setAdapter(recyclerViewAdapter);
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new swipeToDeleteCallback((PhotoDataAdapter)recyclerViewAdapter));
        itemTouchHelper.attachToRecyclerView(recyclerView);
    }

    private void addDefaultItems() {
        PhotoData pd1 = new PhotoData(url1,"Natura");
        photoDataArrayList.add(pd1);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {//this adds items to action bar if there are some
        getMenuInflater().inflate(R.menu.additem_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_additem :
                Intent intent = new Intent(this, AddItemActivity.class);
                startActivityForResult(intent,21);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK){
            if (requestCode == 21){
                String name = data.getStringExtra("ImgName");
                String url = data.getStringExtra("ImgUrl");
                PhotoData newPD = new PhotoData(url,name);
                photoDataArrayList.add(newPD);
                recyclerViewAdapter.notifyDataSetChanged();
            }
        }
    }
}
