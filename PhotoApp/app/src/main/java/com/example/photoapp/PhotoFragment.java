package com.example.photoapp;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

public class PhotoFragment extends Fragment {
    private String url;
    private ImageView imageView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_image, container, false);
        url = getArguments().getString("URL");
        imageView = (ImageView) view.findViewById(R.id.fragmentImageView);
        loadimage();
        return view;
    }

    private void loadimage() {
        Picasso.get().load(url).into(imageView);
    }
}
