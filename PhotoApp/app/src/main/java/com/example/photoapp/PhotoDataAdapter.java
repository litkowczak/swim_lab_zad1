package com.example.photoapp;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.photoapp.logic.PhotoData;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import com.google.firebase.ml.vision.label.FirebaseVisionImageLabel;
import com.google.firebase.ml.vision.label.FirebaseVisionImageLabeler;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;
import java.util.List;

public class PhotoDataAdapter extends RecyclerView.Adapter<PhotoDataAdapter.MyViewHolder> {
    private ArrayList<PhotoData> photoDataArrayList;
    private Context context;
    private Target target;
    private ClickListener clickListener;

    public interface ClickListener {
        void onItemClick(int position, View v);
    }
    public void setOnItemClickListener(ClickListener listener){
        clickListener = listener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.photolist, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder myViewHolder, int i) {
        final PhotoData photoData = photoDataArrayList.get(i);

        target = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                processImage(myViewHolder,photoData,bitmap);
                myViewHolder.photoImageView.setImageBitmap(bitmap);
                Log.wtf("TAG","Ladowanie bitmap");
            }

            @Override
            public void onBitmapFailed(Exception e, Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        };

        Picasso.get().load(photoData.getImgUrl()).into(target);

    }

    private void processImage(final MyViewHolder myViewHolder, final PhotoData photoData, Bitmap bitmap) {
        FirebaseVisionImage fvi = FirebaseVisionImage.fromBitmap(bitmap);
        FirebaseVisionImageLabeler labeler = FirebaseVision.getInstance().getOnDeviceImageLabeler();
        labeler.processImage(fvi)
                .addOnSuccessListener(new OnSuccessListener<List<FirebaseVisionImageLabel>>() {
                    @Override
                    public void onSuccess(List<FirebaseVisionImageLabel> labels) {
                        ArrayList<String> tags = new ArrayList<>();
                        for (FirebaseVisionImageLabel label : labels) {
                            Log.wtf("TAG", label.getText());
                            tags.add(label.getText());
                        }
                        photoData.setTagsArray(tags);
                        myViewHolder.imageTagsTextView.setText(photoData.getTag());
                        myViewHolder.imageDateTextView.setText(photoData.getDate());
                        myViewHolder.imageNameTextView.setText(photoData.getName());
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        // Task failed with an exception
                        // ...
                        Log.wtf("TAG", "Set tag fail");
                    }
                });
    }


    @Override
    public int getItemCount() {
        return photoDataArrayList.size();
    }

    public void deleteItem(int position) {
        photoDataArrayList.remove(position);
        notifyItemRemoved(position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder  implements View.OnClickListener {
        public ImageView photoImageView;
        public TextView imageNameTextView;
        public TextView imageTagsTextView;
        public TextView imageDateTextView;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.photoImageView = (ImageView) itemView.findViewById(R.id.photoImageView);
            this.imageNameTextView = (TextView) itemView.findViewById(R.id.nameTextView);
            this.imageTagsTextView = (TextView) itemView.findViewById(R.id.tagsTextView);
            this.imageDateTextView = (TextView) itemView.findViewById(R.id.dateTextView);
            itemView.setOnClickListener(this);


        }


        @Override
        public void onClick(View v) {
            clickListener.onItemClick(getAdapterPosition(),v);
        }
    }

    public PhotoDataAdapter(ArrayList<PhotoData> photoData, Context context) {
        this.photoDataArrayList = photoData;
        this.context = context;
    }

    public Context getContext() {
        return context;
    }





}
