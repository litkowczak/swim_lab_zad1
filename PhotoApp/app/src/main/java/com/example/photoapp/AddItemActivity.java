package com.example.photoapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.photoapp.logic.PhotoData;



public class AddItemActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_item);

        Button addItemBtn = findViewById(R.id.addBtn);

        addItemBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String name = getImageName();
                String url = getImageUrl();
                if(!name.equals("") && !url.equals("")){

                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("ImgName",name);
                    returnIntent.putExtra("ImgUrl", url);
                    setResult(RESULT_OK,returnIntent);
                    finish();
                }
                else{
                    Toast.makeText(getApplicationContext(),"Something is wrong", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    private String getImageUrl() {
        EditText urlEditText = findViewById(R.id.UrlEditText);

            return urlEditText.getText().toString();

    }

    private String getImageName() {
        EditText imageNameEditText = findViewById(R.id.ImageNameEditText);

            return imageNameEditText.getText().toString();

    }
}
