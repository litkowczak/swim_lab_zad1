package com.example.photoapp;


import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.os.Bundle;

import java.util.ArrayList;

public class DetailsActivity extends FragmentActivity {

    private String url;
    private String name;
    private ArrayList<String> tags;
    private ArrayList<ArrayList<String>> listOfAllTags = new ArrayList<>();
    private int numberOfPhotos = 0;
    private ArrayList<String> urlAddresses = new ArrayList<>();
    private int actualPosition = 0;

    private ViewPager pager;
    private ScreenSlideAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        getDataFromIntent();
        adapter = new ScreenSlideAdapter(getSupportFragmentManager());
        pager = findViewById(R.id.pager);
        setupPager();

    }

    public void setupPager() {
        adapter = new ScreenSlideAdapter(getSupportFragmentManager());
        PhotoFragment frag1 = new PhotoFragment();
        Bundle bundle = new Bundle();
        bundle.putString("URL",url);
        bundle.putString("NAME", name);
        bundle.putStringArrayList("TAGS", tags);

        bundle.putStringArrayList("URLSLIST",findSimilarPhotos());
        frag1.setArguments(bundle);

        DetailsFragment dfrag = new DetailsFragment();
        dfrag.setArguments(bundle);
        adapter.addNewFragment(frag1);
        adapter.addNewFragment(dfrag);

        pager.setAdapter(adapter);
    }

    private void getDataFromIntent() {
        url = getIntent().getStringExtra("URL");
        name = getIntent().getStringExtra("NAME");
        tags = getIntent().getStringArrayListExtra("TAGS");
        numberOfPhotos = getIntent().getIntExtra("NUMBEROFPHOTOS",0);
        actualPosition = getIntent().getIntExtra("POSITION",0);
        for(int i=0;i<numberOfPhotos;i++){
            urlAddresses.add(getIntent().getStringExtra("URLLIST" + i));
            listOfAllTags.add(getIntent().getStringArrayListExtra("TAGLIST"+i));
        }

    }
    //tutaj zrob funkcje ktora szuka podobnych zdjec, po wszystkich tagach i wrzuc te podobne do fragmentu dfrag - details funkcja ma porownywac tagi i zwrocic tablice urli

    public ArrayList<String> findSimilarPhotos () {
        ArrayList<String> similarURLS = new ArrayList<>();
        for(int i=0;i<listOfAllTags.size();i++){ // getting array od tags for one photo
            if(i!=actualPosition){ //index cannot be the same as photo in this fragment
                if(similarItems(listOfAllTags.get(i))){
                    similarURLS.add(urlAddresses.get(i));
                }
            }
        }
        return  similarURLS;
    }

    private boolean similarItems(ArrayList<String> strings) {
        int count = 0;
        int numberOfItems = strings.size() > tags.size() ? tags.size() : strings.size();
        for(int index1 = 0; index1<numberOfItems;index1++){
            for(int index2 = 0 ; index2< numberOfItems;index2++){
                if(tags.get(index1).equals(strings.get(index2))){
                    count++;
                }
            }
        }
        return count >=2 ? true : false;
    }
}

