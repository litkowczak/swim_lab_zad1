package com.example.bmi


import android.view.View
import android.view.ViewGroup
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.Espresso.pressBack
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers.allOf
import org.hamcrest.TypeSafeMatcher
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@LargeTest
@RunWith(AndroidJUnit4::class)
class MainActivityTest {

    @Rule
    @JvmField
    var mActivityTestRule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun mainActivityTest() {
        val appCompatEditText = onView(

                withId(R.id.masseditText))

        appCompatEditText.perform(click())

        val appCompatEditText2 = onView(

                withId(R.id.masseditText))

        appCompatEditText2.perform(replaceText("60"), closeSoftKeyboard())



        val appCompatEditText3 = onView(

                withId(R.id.heighteditText))

        appCompatEditText3.perform(replaceText("170"), closeSoftKeyboard())



        val appCompatButton = onView(

                withId(R.id.coutnBTN))
        appCompatButton.perform(click())

        val textView = onView(withId(R.id.resultTextView))
        textView.check(matches(withText("20,76")))
    }


}
