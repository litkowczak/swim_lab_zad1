package com.example.bmi

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_about_me.*

class AboutMe : AppCompatActivity() {

    private var number : Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTitle(R.string.aboutme)
        setContentView(R.layout.activity_about_me)

        ClickBtn.setOnClickListener {
            number++
            numberTV.text = number.toString()
        }
    }
}
