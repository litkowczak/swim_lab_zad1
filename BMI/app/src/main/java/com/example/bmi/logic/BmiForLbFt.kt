package com.example.bmi.logic

class BmiForLbFt (var mass: Double, var height : Double) : Bmi {
    override fun countBmi() = mass*703.0/((height*12.0)*(height*12.0))
}