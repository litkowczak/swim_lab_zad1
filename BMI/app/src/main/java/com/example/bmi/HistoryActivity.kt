package com.example.bmi

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_history.*

class HistoryActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTitle(R.string.history)
        setContentView(R.layout.activity_history)


        val data  : ArrayList<String> = intent.getStringArrayListExtra(HISTORY)



        history_recyclerview.layoutManager = LinearLayoutManager(this)
        history_recyclerview.adapter = RecyclerViewAdapter(data)


    }


}

