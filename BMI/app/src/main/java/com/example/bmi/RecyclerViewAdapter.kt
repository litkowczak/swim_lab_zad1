package com.example.bmi

import android.view.LayoutInflater
import android.view.TextureView
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.view.menu.ActionMenuItemView
import androidx.appcompat.view.menu.MenuView
import androidx.recyclerview.widget.RecyclerView
import com.example.bmi.logic.BmiHistory

class RecyclerViewAdapter (private val myHistory : ArrayList<String>) : RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewAdapter.ViewHolder {
        val view : View = LayoutInflater.from(parent.context).inflate(R.layout.layout_listitem, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount() = myHistory.size


    override fun onBindViewHolder(holder: RecyclerViewAdapter.ViewHolder, position: Int) {
        val split = myHistory[position].split(";")
        holder.massTextView.text = split[0]
        holder.heightTextView.text = split[1]
        holder.dateTextView.text = split[2]
        holder.resultTextView.text = split [3]

    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val massTextView : TextView = itemView.findViewById(R.id.masshistoryTV)
        val heightTextView : TextView = itemView.findViewById(R.id.heighthistoryTV)
        val dateTextView : TextView = itemView.findViewById(R.id.dateHistoryTV)
        val resultTextView : TextView = itemView.findViewById(R.id.resulthistoryTV)

    }

}