package com.example.bmi

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color.*
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.preference.PreferenceManager
import android.widget.Toast
import com.example.bmi.logic.BmiForKgCm
import kotlinx.android.synthetic.main.activity_main.*
import android.view.Menu
import android.view.MenuItem
import com.example.bmi.logic.BmiForLbFt
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashSet

const val  STATUS = "Status"
const val NUMBEROFELEMENTS = "NumberOfElements"
const val  HISTORY = "history"
const val UNITSEUROPEAN = 0
const val UNITSIMPERIAL = 1
const val NUMBEROFUNITS = 2
const val SEMICOLON = ";"

class MainActivity : AppCompatActivity() {


    private var result = 0.0
    private var units = 0
    private var historyOfResults: ArrayList<String> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        loadHistory(this)

        coutnBTN.setOnClickListener {
            var mass_string: String = masseditText.text.toString()
            var height_string: String = heighteditText.text.toString()

            if (mass_string != "" && height_string != "") {
                var mass = mass_string.toDouble()
                var height = height_string.toDouble()
                var isCoutned = false
                isCoutned = calculateBMI(mass, height)
                if (isCoutned) {
                    setMsgTextView(result)
                    setResultTextView(result)
                    addToHistory(mass_string, height_string, result)
                    saveHistory()
                } else {
                    handleWrongData()
                }
            } else {
                handleWrongData()
                result = 0.0
                resultTextView.setTextColor(BLACK)
            }
        }
        detailsBtn.setOnClickListener {
            val intent = Intent(this, Activity2::class.java).apply {
                putExtra(getString(R.string.bmi_from_main), result)
            }
            startActivity(intent)
        }


    }

    private fun calculateBMI(mass: Double, height: Double): Boolean {
        if(units == UNITSEUROPEAN && mass in 0.0..200.0 && height in 0.0..300.0) {
            result = BmiForKgCm(mass.toInt(), height.toInt()).countBmi()
            return true
        } else if (units == UNITSIMPERIAL && mass in 0.0..400.0 && height in 0.0..10.0) {
            result = BmiForLbFt(mass, height).countBmi()
            return true
        } else {
            result = 0.0
            return false
        }
    }

    private fun addToHistory(mass_string: String, height_string: String, result: Double) {
        val sdf = SimpleDateFormat("dd/MM/yyyy")
        val currentDate = sdf.format(Date())

        if (historyOfResults.size >= 10) {
            historyOfResults.clear()
            val sharedPref = PreferenceManager.getDefaultSharedPreferences(this)
            val editor = sharedPref.edit()
            editor.clear()
            editor.commit()
        }
        val sb = StringBuilder()

        if (units == UNITSEUROPEAN) {
            sb.append(mass_string).append("[kg]").append(SEMICOLON).append(height_string).append("[cm]").append(
                SEMICOLON)
        } else if (units == UNITSIMPERIAL) {
            sb.append(mass_string).append("[lb]").append(SEMICOLON).append(height_string).append("[ft]").append(
                SEMICOLON)
        }
        sb.append(currentDate).append(SEMICOLON).append(String.format("%.2f", result))
        val readyString = sb.toString()
        historyOfResults.add(readyString)

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == R.id.AboutMeBtn) {
            openActivityAboutMe()
            return true
        }
        if (item?.itemId == R.id.ChangeUnitsBtn) {
            changeUnits()
            return true
        }
        if (item?.itemId == R.id.HistoryBtn) {
            openHistory()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private fun openHistory() {
        val intent = Intent(this, HistoryActivity::class.java).apply {
            putExtra(HISTORY , historyOfResults)
        }
        startActivity(intent)
    }

    private fun setMsgTextView(bmiresult: Double) {
        when (bmiresult) {
            in Double.MIN_VALUE..18.5 -> {
                msgTextView.text = getString(R.string.underweight_txt); msgTextView.setTextColor(YELLOW)
            }
            in 18.5..24.9 -> {
                msgTextView.text = getString(R.string.Normal_txt); msgTextView.setTextColor(resources.getColor(R.color.lapisLazuli))
            }
            in 24.9..29.9 -> {
                msgTextView.text = getString(R.string.overweight_txt); msgTextView.setTextColor(resources.getColor(R.color.grynszpan))
            }
            in 29.9..Double.MAX_VALUE -> {
                msgTextView.text = getString(R.string.obese_txt); msgTextView.setTextColor(resources.getColor(R.color.rozPompejski))
            }
        }
    }

    private fun handleWrongData() {
        Toast.makeText(applicationContext, getString(R.string.toasterrmsg), Toast.LENGTH_SHORT).show()
        resultTextView.text = getString(R.string.defaultvaluebmi)
        resultTextView.setTextColor(BLACK)
        msgTextView.text = getString(R.string.errmsg)
        msgTextView.setTextColor(RED)
    }

    private fun setResultTextView(bmiresult: Double) {
        resultTextView.text = (String.format("%.2f", bmiresult))

        when (bmiresult) {
            in Double.MIN_VALUE..18.5 -> {
                resultTextView.setTextColor(YELLOW)
            }
            in 18.5..24.9 -> {
                resultTextView.setTextColor(resources.getColor(R.color.lapisLazuli))
            }
            in 24.9..29.9 -> {
                resultTextView.setTextColor(resources.getColor(R.color.grynszpan))
            }
            in 29.9..Double.MAX_VALUE -> {
                resultTextView.setTextColor(resources.getColor(R.color.rozPompejski))
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        outState?.run {
            putString(BMI_RESULT, resultTextView.text.toString())
            putString(BMI_RESULT_TEXT, msgTextView.text.toString())
            putDouble(BMI_RESULT_DOUBLE, result)
            putInt(BMI_UNITS, units)
            putString(MASS_TEXTV, textView.text.toString())
            putString(HEIGHT_TEXT, textView2.text.toString())

        }
        super.onSaveInstanceState(outState)
    }

    companion object {
        val BMI_RESULT = "bmiResult"
        val BMI_RESULT_TEXT = "bmiText"
        val BMI_RESULT_DOUBLE = "bmiDouble"
        val BMI_UNITS = "bmiUnits"
        val MASS_TEXTV = "massText"
        val HEIGHT_TEXT = "heightText"
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)

        savedInstanceState?.run {
            resultTextView.text = getString(BMI_RESULT)
            msgTextView.text = getString(BMI_RESULT_TEXT)
            result = getDouble(BMI_RESULT_DOUBLE)
            units = getInt(BMI_UNITS)
            textView.text = getString(MASS_TEXTV)
            textView2.text = getString(HEIGHT_TEXT)
        }
        setMsgTextView(result)
        setResultTextView(result)
    }

    private fun openActivityAboutMe() {
        val intent = Intent(this, AboutMe::class.java)
        startActivity(intent)

    }

    private fun changeUnits() {
        if (units == UNITSEUROPEAN) {
            textView.text = getString(R.string.mass_lb)
            textView2.text = getString(R.string.height_ft)
            units = (units + 1) % NUMBEROFUNITS
        } else if (units == UNITSIMPERIAL) {
            textView.text = getString(R.string.mass_kg)
            textView2.text = getString(R.string.height_cm)
            units = (units + 1) % NUMBEROFUNITS
        }
    }

    private fun loadHistory(mContext: Context) {
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(mContext)
        historyOfResults.clear()
        var size = sharedPref.getInt(NUMBEROFELEMENTS, 0)
        for (i in 0..size - 1) {
            historyOfResults.add(sharedPref.getString(STATUS + i, null))
        }
    }

    private fun saveHistory() {
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(this)
        val editor = sharedPref.edit()
        editor.putInt(NUMBEROFELEMENTS, historyOfResults.size)

        for (i in 0..historyOfResults.size - 1) {
            editor.remove(STATUS + i)
            editor.putString(STATUS + i, historyOfResults[i])
        }
        editor.commit()
    }


}
