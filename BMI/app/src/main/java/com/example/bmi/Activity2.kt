package com.example.bmi

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_2.*


class Activity2 : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTitle(R.string.info)
        setContentView(R.layout.activity_2)

        val result = intent.getDoubleExtra("BMI_main_activity", 0.0)

        setMessage(result)

    }

    private fun setMessage (res : Double) {
        resultText.text =  (String.format("%.2f", res))

        when (res) {
            in Double.MIN_VALUE..18.5 -> {
                textView5.setTextColor(Color.YELLOW)
                textView5.text = "You are underweight, eat more !"
            }
            in 18.5..24.9 -> {
                textView5.setTextColor(resources.getColor(R.color.lapisLazuli))
                textView5.text = "Everything is normal"

            }
            in 24.9..29.9 -> {
                textView5.setTextColor(resources.getColor(R.color.grynszpan))
                textView5.text = "Eat a little bit less"
            }
            in 29.9..Double.MAX_VALUE -> {
                textView5.setTextColor(resources.getColor(R.color.rozPompejski))
                textView5.text = "Start doing some activities immidietly"
            }
        }
    }
}
