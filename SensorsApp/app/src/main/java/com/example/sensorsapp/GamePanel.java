package com.example.sensorsapp;


import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import java.util.ArrayList;

class GamePanel extends SurfaceView implements SurfaceHolder.Callback {
    private GameThread thread;
    private PlayerObject playerObject;
    private Point playerPoint;
    private int Points = 0;
    private Rect r = new Rect();
    private ArrayList<Star> stars;


    private SensorData sensorData;
    private long frameTime;

    private boolean gameOver = false;
    private int BackgroundColor = Color.WHITE;

    public GamePanel(Context context) {
        super(context);

        getHolder().addCallback(this);
        thread = new GameThread(getHolder(), this);


        playerObject = new PlayerObject(new Point(Constants.OBJECTSIZE), this.getContext());
        playerPoint = new Point(Constants.BALLPOSITION);
        stars = new ArrayList<>();

        stars.add(new Star(new Point(Constants.OBJECTSIZE),this.getContext(), Constants.FIRSTSTARPOSITION));
        stars.add(new Star(new Point(Constants.OBJECTSIZE),this.getContext(), Constants.SNDSTARPOSITION));
        stars.add(new Star(new Point(Constants.OBJECTSIZE),this.getContext(), Constants.THIRDSTARPOSITION));
        stars.add(new Star(new Point(Constants.OBJECTSIZE),this.getContext(),Constants.FORTHSTARPOSITION));
        stars.add(new Star(new Point(Constants.OBJECTSIZE),this.getContext(),Constants.FIFTHSTARPOSITION));
        stars.add(new Star(new Point(Constants.OBJECTSIZE), this.getContext(),Constants.SIXTHSTARPOSITON));
        stars.add(new Star(new Point(Constants.OBJECTSIZE),this.getContext(), Constants.SEVENTHSTARPOSITION));

        sensorData = new SensorData();
        sensorData.register();
        frameTime = System.currentTimeMillis();

        setFocusable(true);
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        thread = new GameThread(getHolder(), this);
        Constants.INIT_TIME = System.currentTimeMillis();
        thread.setRunning(true);
        thread.start();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        boolean retry = true;
        while(retry){
            try{
                thread.setRunning(false);
                thread.join();
            }catch (Exception e){e.printStackTrace();}
            retry = false;
        }
    }

    public void update(){
        if(!gameOver) {

            getMovementInformation();

            checkIfBallIsOutOfBounds();

            checkStarCollision();

            checkIfLightChanged();

            if(Points == stars.size()) gameOver = true;

            //Log.w("TAG","POINT X : " + playerPoint.x + " POINT Y : " + playerPoint.y);
            playerObject.update(playerPoint);

        }
    }
    public void checkStarCollision() {
        for(int i = 0; i<stars.size();i++){
            if(stars.get(i).starCollide(playerObject)){
                stars.get(i).update();
                Points++;
            }
        }
    }
    public void checkIfLightChanged() {
        //Log.wtf("TAG", "LIGHT VALUE : " + sensorData.getLightoutput());
        if(sensorData.getLightoutput() <= 5.0f){
            BackgroundColor = Color.GREEN;
        }
        else {
            BackgroundColor = Color.WHITE;
        }
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        canvas.drawColor(BackgroundColor);
        playerObject.draw(canvas);
        for(int i = 0;i<stars.size();i++){
            stars.get(i).draw(canvas);
        }
        Paint paint1 = new Paint();
        paint1.setTextSize(30);
        paint1.setColor(Color.BLACK);
        drawPointsText(canvas,paint1, "Points : " + Points);
        if(gameOver){
            Paint paint = new Paint();
            paint.setTextSize(100);
            paint.setColor(Color.RED);
            drawCenterText(canvas,paint,"GAME DONE");
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()){
            case MotionEvent.ACTION_DOWN :
                reset();
                gameOver = false;

        }
        return super.onTouchEvent(event);
    }

    public void reset() {
        playerPoint = new Point(Constants.BALLPOSITION);
        playerObject.update(playerPoint);
        for(int i=0;i<stars.size();i++){
            stars.get(i).update(Constants.STARSPOSITIONS[i]);
        }
        Points = 0;

    }

    private void drawCenterText(Canvas canvas, Paint paint, String text) {
        paint.setTextAlign(Paint.Align.LEFT);
        canvas.getClipBounds(r);
        int cHeight = r.height();
        int cWidth = r.width();
        paint.getTextBounds(text, 0, text.length(), r);
        float x = cWidth / 2f - r.width() / 2f - r.left;
        float y = cHeight / 2f + r.height() / 2f - r.bottom;
        canvas.drawText(text, x, y, paint);

    }
    private void drawPointsText(Canvas canvas, Paint paint, String text) {
        paint.setTextAlign(Paint.Align.LEFT);
        canvas.getClipBounds(r);
        int cHeight = r.height()/30;
        int cWidth = r.width();
        paint.getTextBounds(text, 0, text.length(), r);
        float x = cWidth / 2f - r.width() / 2f - r.left;
        float y = cHeight / 2f + r.height() / 2f - r.bottom;
        canvas.drawText(text, x, y, paint);

    }
    private void getMovementInformation(){
        int elapsedTime = (int) (System.currentTimeMillis() - frameTime);
        frameTime = System.currentTimeMillis();

        if (sensorData.getOrientation() != null && sensorData.getStartOrientation() != null) {
            float pitch = sensorData.getOrientation()[1] - sensorData.getStartOrientation()[1];
            float roll = sensorData.getOrientation()[2] - sensorData.getStartOrientation()[2];

            float x_speed = 2 * roll * Constants.SCREEN_WIDTH / 1000f;
            float y_speed = pitch * Constants.SCREEN_HEIGHT / 1000f;

            playerPoint.x += Math.abs((x_speed * elapsedTime)) > 8 ? x_speed * elapsedTime : 0;
            playerPoint.y -= Math.abs(y_speed * elapsedTime) > 8 ? y_speed * elapsedTime : 0;
        }

    }
    private void checkIfBallIsOutOfBounds() {
        if (playerPoint.x < playerObject.getBallSize().x / 2)
            playerPoint.x = playerObject.getBallSize().x / 2;
        else if (playerPoint.x + playerObject.getBallSize().x / 2 > Constants.SCREEN_WIDTH)
            playerPoint.x = Constants.SCREEN_WIDTH - playerObject.getBallSize().x / 2;

        if (playerPoint.y < playerObject.getBallSize().y / 2)
            playerPoint.y = playerObject.getBallSize().y / 2;
        else if (playerPoint.y + playerObject.getBallSize().y / 2 > Constants.SCREEN_HEIGHT)
            playerPoint.y = Constants.SCREEN_HEIGHT - playerObject.getBallSize().y / 2;

    }

}
