package com.example.sensorsapp;

import android.content.Context;
import android.graphics.Point;

public class Constants {
    public static int SCREEN_WIDTH;
    public static int SCREEN_HEIGHT;
    public static Context CURRENT_CONTEXT;
    public static long INIT_TIME;

    public static Point FIRSTSTARPOSITION = new Point(550,50);
    public static Point SNDSTARPOSITION = new Point(150,500);
    public static Point THIRDSTARPOSITION = new Point(400,1000);
    public static Point FORTHSTARPOSITION = new Point(420,465);
    public static Point FIFTHSTARPOSITION = new Point(300,225);
    public static Point SIXTHSTARPOSITON = new Point(100,1000);
    public static Point SEVENTHSTARPOSITION = new Point(600,680);

    public static Point [] STARSPOSITIONS = {FIRSTSTARPOSITION,SNDSTARPOSITION
    ,THIRDSTARPOSITION,FORTHSTARPOSITION,FIFTHSTARPOSITION,SIXTHSTARPOSITON, SEVENTHSTARPOSITION};

    public static Point BALLPOSITION = new Point(150,150);
    public static Point OBJECTSIZE = new Point(50,50);
}
