package com.example.sensorsapp;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.Rect;
import android.util.Log;

public class Star implements GameObject {

    private Point starSize;
    private Point starPosition;
    private Bitmap starBitmap;
    private Context context;

    public Star(Point point, Context context, Point starPosition) {
        this.starSize = point;
        this.context = context;
        this.starPosition = starPosition;
        starBitmap = BitmapFactory.decodeResource(context.getResources(),R.drawable.bitstar);

    }

    @Override
    public void draw(Canvas canvas) {
        canvas.drawBitmap(Bitmap.createScaledBitmap(starBitmap,starSize.x,starSize.y,true),
                starPosition.x,starPosition.y  ,null);
    }

    public boolean starCollide(PlayerObject object) {
        int x = object.getBallPosition().x;
        int y = object.getBallPosition().y;
        int ballSize = object.getBallSize().x/2;
        Rect rect = new Rect(starPosition.x-starSize.x/2, starPosition.y-starSize.y/2,
                starPosition.x+starSize.x/2, starPosition.y+starSize.y/2);
        Rect ballRect = new Rect(x - ballSize,y-ballSize,x+ballSize,y+ballSize);
        if(rect.intersect(ballRect)){
            //Log.wtf("TAG","COLLISION");
            return true;
        }
        return false;
    }

    @Override
    public void update() {
        this.starPosition = new Point( -100, -100);
    }

    public void update(Point newPosition){
        this.starPosition = new Point(newPosition);
    }

}
