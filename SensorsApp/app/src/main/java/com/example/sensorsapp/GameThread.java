package com.example.sensorsapp;

import android.graphics.Canvas;
import android.util.Log;
import android.view.SurfaceHolder;

class GameThread extends Thread {
    public static final int MAX_FPS = 30;
    private double averageFPS;
    private SurfaceHolder surfaceHolder;
    private GamePanel gamePanel;
    private boolean isRunning;
    public static Canvas canvas;

    public GameThread(SurfaceHolder surfaceHolder, GamePanel gamePanel) {
        super();
        this.surfaceHolder = surfaceHolder;
        this.gamePanel = gamePanel;
    }

    @Override
    public void run() {
        super.run();
        long startTime;
        long timeMIlis = 1000/MAX_FPS;
        long waitTime;
        long frameCount = 0;
        long totalTime = 0;
        long targetTime = 1000/MAX_FPS;

        while(isRunning){
            startTime = System.nanoTime();
            canvas = null;
            try {
                canvas = this.surfaceHolder.lockCanvas();
                synchronized (surfaceHolder){
                    this.gamePanel.update();//przesuwanie obiektow
                    this.gamePanel.draw(canvas);//patrzenie na cordynaty i update
                }
            }catch (Exception e ){e.printStackTrace();}
            finally {
                if(canvas != null){
                    try{
                        surfaceHolder.unlockCanvasAndPost(canvas);
                    }catch (Exception e ){
                        e.printStackTrace();
                    }
                }
            }
            timeMIlis = (System.nanoTime() - startTime)/ 1000000;
            waitTime = targetTime - timeMIlis;

            try {
                if(waitTime > 0 ){
                    this.sleep(waitTime);
                }
            }catch (Exception e ){
                e.printStackTrace();
            }
            totalTime += System.nanoTime() - startTime;
            frameCount++;
            if(frameCount == MAX_FPS){
                averageFPS = 1000/((totalTime/frameCount)/1000000);
                frameCount = 0;
                totalTime = 0;
                //Log.wtf("TAG", Double.toString(averageFPS));
            }
        }
    }

    public void setRunning(boolean isRunning){
        this.isRunning = isRunning;
    }
}
