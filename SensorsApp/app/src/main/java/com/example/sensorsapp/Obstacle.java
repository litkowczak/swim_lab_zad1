package com.example.sensorsapp;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.util.Log;

public class Obstacle implements GameObject {
    private Rect rectangle;
    private int color;

    public Obstacle(Rect rectangle, int color) {
        this.rectangle = rectangle;
        this.color = color;
    }

    public boolean playerCollide(PlayerObject object, int x, int y) {
        int objectSizeX = object.getBallSize().x;
        int objectSizeY = object.getBallSize().y;
        int objectCordX = object.getBallPosition().x;
        int objectCordY = object.getBallPosition().y;

        if(rectangle.contains( x+ objectSizeX/2, y)||
            rectangle.contains(x-objectSizeX/2,y)||
            rectangle.contains(x,y+objectSizeY/2)||
            rectangle.contains(x,y-objectSizeY/2)){
            Log.wtf("TAG","COLLISION");
            return true;
        }
        return false;
    }

    @Override
    public void draw(Canvas canvas) {
        Paint paint = new Paint();
        paint.setColor(color);
        canvas.drawRect(rectangle,paint);
    }

    @Override
    public void update() {

    }
}
