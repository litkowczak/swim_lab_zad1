package com.example.sensorsapp;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Point;
import android.util.Log;

public class PlayerObject implements GameObject {

    private Context context;
    private Point ballSize;
    private Point ballPosition;
    private Bitmap ballBitmap;

    public PlayerObject(Point point, Context context) {
    this.ballSize = point;
    this.context = context;
    ballPosition = new Point();
    ballBitmap = BitmapFactory.decodeResource(context.getResources(),R.drawable.ball);

    }

    @Override
    public void draw(Canvas canvas) {
        canvas.drawBitmap(Bitmap.createScaledBitmap(ballBitmap,ballSize.x,ballSize.y,true),
                ballPosition.x - ballSize.x/2 ,ballPosition.y - ballSize.y/2 ,null);
    }

    @Override
    public void update() {

    }

    public void update(Point point) {
        this.ballPosition.x = point.x;
        this.ballPosition.y = point.y;
        //Log.wtf("TAG", point.x + " " + point.y);
    }

    public Point getBallPosition() {
        return ballPosition;
    }

    public Point getBallSize() {
        return ballSize;
    }
}
