package com.example.musicplayer;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class SongAdapter extends BaseAdapter {
    private ArrayList<Song> songList;
    private LayoutInflater songInf;
    @Override
    public int getCount() {
        return songList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LinearLayout songLayout = (LinearLayout) songInf.inflate(R.layout.song,parent,false);
        TextView songTextView = songLayout.findViewById(R.id.song_title);
        TextView artistTextView = songLayout.findViewById(R.id.song_artist);
        Song currentSong = songList.get(position);
        songTextView.setText(currentSong.getSongTitle());
        artistTextView.setText(currentSong.getSongArtist());
        songLayout.setTag(position);
        return songLayout;
    }

    public SongAdapter(Context c, ArrayList<Song> theSongList){
        this.songList = theSongList;
        songInf = LayoutInflater.from(c);
    }
}
