package com.example.musicplayer;

import android.app.Service;
import android.content.ContentUris;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.os.PowerManager;
import android.provider.MediaStore;
import android.app.Notification;
import android.app.PendingIntent;

import java.util.ArrayList;

public class MusicService extends Service implements MediaPlayer.OnPreparedListener,
        MediaPlayer.OnErrorListener,
        MediaPlayer.OnCompletionListener {

    private MediaPlayer mediaPlayer;
    private ArrayList<Song> songList;
    private int songPosition;

    private final IBinder musicBind = new MusicBinder();

    private String songTitle = "";
    private static final int NOTIFY_ID = 1;


    @Override
    public void onCreate() {
        super.onCreate();
        songPosition = 0;
        mediaPlayer = new MediaPlayer();
        initMusicPlayer();
    }

    public void initMusicPlayer() {
        mediaPlayer.setWakeMode(getApplicationContext(), PowerManager.PARTIAL_WAKE_LOCK);
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mediaPlayer.setOnPreparedListener(this);
        mediaPlayer.setOnCompletionListener(this);
        mediaPlayer.setOnErrorListener(this);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return musicBind;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        mediaPlayer.stop();
        mediaPlayer.release();
        return false;
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        mp.reset();
        playNext();
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        mp.reset();
        return false;
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        mp.start();
        Intent notIntent = new Intent(this, MainActivity.class);
        notIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,0,notIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        Notification.Builder builder = new Notification.Builder(this);
        builder.setContentIntent(pendingIntent).setSmallIcon(R.drawable.ic_action_name)
                .setTicker(songTitle)
                .setOngoing(true)
                .setContentTitle("Playing :")
                .setContentText(songTitle);
        Notification not = builder.build();
        startForeground(NOTIFY_ID, not);
    }

    public void setSongList(ArrayList<Song> songList) {
        this.songList = songList;
    }

    public class MusicBinder extends Binder {
        MusicService getService() {
            return MusicService.this;
        }
    }

    public void setSong(int songIndex){
        songPosition = songIndex;
    }

    public void playSong(){
        mediaPlayer.reset();
        Song currentSong = songList.get(songPosition);
        songTitle = currentSong.getSongTitle();
        long currSong = currentSong.getSongID();
        Uri trackUri = ContentUris.withAppendedId(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,currSong);

        try{
            mediaPlayer.setDataSource(getApplicationContext(),trackUri);
        }catch (Exception e){

        }
        mediaPlayer.prepareAsync();
    }

    public int getSongPosition(){
        return mediaPlayer.getCurrentPosition();
    }
    public int getDuration(){
        return mediaPlayer.getDuration();
    }
    public boolean isPlaying(){
        return mediaPlayer.isPlaying();
    }
    public  void pausePlayer() {
        mediaPlayer.pause();
    }
    public void seek(int position){
        mediaPlayer.seekTo(position);
    }
    public void go() {
        mediaPlayer.start();
    }

    public void playPrevious()  {
        songPosition--;
        if(songPosition<0) songPosition = songList.size()-1;
        playSong();
    }

    public void playNext(){
        songPosition++;
        if(songPosition >= songList.size()) songPosition = 0;
        playSong();
    }

    @Override
    public void onDestroy() {
        stopForeground(true);
    }
}
