package com.example.mywearapp;

import android.graphics.drawable.Drawable;
import android.media.Image;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.view.MotionEventCompat;
import android.support.wearable.activity.WearableActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends WearableActivity {

    private ImageView mainImageView;
    private ArrayList<Drawable> imagesToDisplay;
    private int indexOfPhoto = 0;
    private ConstraintLayout myLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mainImageView = findViewById(R.id.imageView2);
        myLayout = findViewById(R.id.mainLayout);

        myLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                indexOfPhoto++;
                if (indexOfPhoto>imagesToDisplay.size()-1) indexOfPhoto = indexOfPhoto%imagesToDisplay.size();
                mainImageView.setImageDrawable(imagesToDisplay.get(indexOfPhoto));
            }
        });

        addImagesToArray();
        mainImageView.setImageDrawable(imagesToDisplay.get(indexOfPhoto));



        // Enables Always-on
        setAmbientEnabled();
    }

    private void addImagesToArray() {
        imagesToDisplay = new ArrayList<>();
        Drawable firstImage = getDrawable(R.drawable.photo1);
        Drawable sndImage = getDrawable(R.drawable.picture2);
        Drawable thirdImage = getDrawable(R.drawable.picture3);
        imagesToDisplay.add(firstImage);
        imagesToDisplay.add(sndImage);
        imagesToDisplay.add(thirdImage);

    }

}
